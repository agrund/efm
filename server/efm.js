Meteor.startup(function () {
	//create a new match if it doesn't exist
	var newMatch = Matches.findOne({state:"new"});
	if (!newMatch) {
		Matches.insert({state:"new"});
	}
});

/* Matches collection
	{
	_id:default
	state:(new|started|ended)
	}
*/
Matches = new Meteor.Collection("matches");
Matches.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  },
  fetch: []
});

/* Match Players collection
	{
	_id:default
	matchId:Match._id
	playerId:Meteor.user()._id
	goals:number of goals
	pos:position (a1=team 1 attack, d1=team 1 defence, a2, d2)
	}
*/
MatchPlayers = new Meteor.Collection("matchPlayers");
MatchPlayers.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  },
  fetch: []
});

//Publish
Meteor.publish("meteor.users", function () {
	return Meteor.users.find({}, {fields:{profile:1}});
});
Meteor.publish("matches", function() {
	return Matches.find();
});
Meteor.publish("matchPlayers", function() {
	return MatchPlayers.find();
});
