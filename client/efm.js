Template.main.loading = function () {
	return !matchesHandle || !matchesHandle.ready() ||
		!matchPlayersHandle || !matchPlayersHandle.ready() ||
		!usersHandle || !usersHandle.ready()
	;
};

Template.page.a1name = function() {
	return getPlayerName("a1");
};
Template.page.d1name = function() {
	return getPlayerName("d1");
};
Template.page.a2name = function() {
	return getPlayerName("a2");
};
Template.page.d2name = function() {
	return getPlayerName("d2");
};
Template.page.matchStarted = function() {
	var currentMatchId = getMatchId();
	var currentMatch = Matches.findOne({_id:currentMatchId});
	return currentMatch.state === "started";
};
Template.page.team1Score = function() {
	return getTeamScore(1);
};
Template.page.team2Score = function() {
	return getTeamScore(2);
};

//team number = 1 | 2
getTeamScore = function getTeamScore(teamNumber) {
	var currentMatchId = getMatchId();
	var currentMatchMatchPlayers = MatchPlayers.find({matchId:currentMatchId});
	var teamScore = 0;
	currentMatchMatchPlayers.forEach(function(mp) {
		if (mp.position === "a"+teamNumber || mp.position === "d"+teamNumber) {
			teamScore += mp.goals;
		}
	});
	return teamScore;
};

Template.page.canStartMatch = function() {
	var matchId = getMatchId();
	var matchPlayers = MatchPlayers.find({
		matchId:matchId
	});

	if (matchPlayers.count() == 1) {
		return "";
	}

	return "disabled";
};

Template.page.getMatchRequiredPlayersCount = function() {
	var matchId = getMatchId();
	var matchPlayers = MatchPlayers.find({
		matchId:matchId
	});

	if (matchPlayers.count() == 4) {
		return "";
	}

	if (matchPlayers.count() == 3) {
		return " - 1 player left";
	}

	return " - " + (4 - matchPlayers.count()) + " players left";
};

getPlayerName = function getPlayerName(position) {
	var matchPlayer = getPlayer(position);

	if (matchPlayer) {
		var player = Meteor.users.findOne({_id:matchPlayer.playerId});

		if (player) {
			return player.profile.name;
		}
	}

	return "JOIN HERE!!!";
};

function getPlayer(position) {
	var matchId = getMatchId();
	var matchPlayer = MatchPlayers.findOne({
		matchId:matchId,
		position:position
	});

	return matchPlayer;
}

Template.page.events({
	"click .avatar": function() {
		if (Meteor.user()) {
			var positionId = event.currentTarget.id;
			var matchId = getMatchId();
			var playerId = Meteor.userId();
			var matchPlayer = MatchPlayers.findOne({matchId:matchId, playerId:playerId});

			if (matchPlayer && matchPlayer.position != positionId) {
				return;
			}

			matchPlayer = getPlayer(positionId);

			if (matchPlayer) {
				var matchPlayerId = MatchPlayers.findOne({matchId:matchId, playerId:playerId, position:positionId})._id;
				MatchPlayers.remove(matchPlayerId);
			} else {
				MatchPlayers.insert({
					matchId:matchId,
					playerId:playerId,
					goals:0,
					position:positionId
				});
			}
		}
	},
	"click #start-match": function() {
		Matches.update({_id:getMatchId()}, {$set:{state:"started"}});
	},
	"click #i-scored": function() {
		var matchId = getMatchId();
		var playerId = Meteor.userId();
		var mp = MatchPlayers.findOne({matchId:matchId, playerId:playerId});
		mp.goals++;
		MatchPlayers.update({_id:mp._id}, mp);
	},
	"click #end-match": function() {
		Matches.update({_id:getMatchId()}, {$set:{state:"ended"}});
		var newMatchId = Matches.insert({state:"new"});
	}
});

getMatchId = function getMatchId() {
	match = Matches.findOne({state: {$in: ["new", "started"]}});
	return match._id;
};

//STARTUP
Meteor.startup(function() {
	//
});

//Collections
Matches = new Meteor.Collection("matches");
MatchPlayers = new Meteor.Collection("matchPlayers");

//Subscribe
var usersHandle = Meteor.subscribe("meteor.users");
var matchesHandle = Meteor.subscribe("matches");
var matchPlayersHandle = Meteor.subscribe("matchPlayers");
